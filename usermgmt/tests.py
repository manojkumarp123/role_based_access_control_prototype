from .models import User
from content.models import Question
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase
from django.utils import timezone
from datetime import timedelta
from content.utils import ConceptGraph, Activity


class UserAPITestCase(APITestCase):
    def setUp(self):
        self.learner = self.create_learner()
        self.author = self.create_author()

    def create_learner(self):
        learner = User.objects.create_user("test", password="entrayn1234")
        learner.entitlements.create(
            valid_upto=timezone.now() + timedelta(days=1),
            concept=ConceptGraph.GRE, activity=Activity.LEARN,
            quantity=3)
        return learner

    def create_author(self):
        author = User.objects.create_user("author", password="entrayn1234")
        author.entitlements.create(
            valid_upto=timezone.now() + timedelta(days=1),
            concept=ConceptGraph.QUANT, activity=Activity.TEACH,
            quantity=3)
        return author

    def learner_must_not_be_able_to_author(self):
        pass

    def learner_must_be_able_to_access_question_under_his_entitled_concept(self):
        pass

    def test_author_able_to_post_under_his_concept(self):
        url = reverse('question-list')
        data = {'answer_choices': [
            {'text': 'a', 'correct': True, 'id': 0},
            {'text': 'b', 'correct': False, 'id': 1},
            {'text': 'a', 'correct': False, 'id': 2},
            {'text': 'b', 'correct': False, 'id': 3}],
            'text': 'test', 'assigned_to': self.learner.id,
            "subject": ConceptGraph.ARITHEMETIC}
        self.client.force_authenticate(self.author)
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Question.objects.count(), 1)
        self.assertEqual(Question.objects.get().text, 'test')

    def test_author_without_GRE_permissions_not_able_to_post(self):
        pass


class UserSignUpAPITestCase(APITestCase):
    def test_user_signup(self):
        url = reverse('user-list')
        data = {'username': "jhon", "password": "HXde.<"}
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        user_id = response.data["id"]
        self.assertEqual(User.objects.get().username, "jhon")

        auth = self.login("jhon", "HXde.<")

        self.Test_user_update_password(user_id, "NHG0(.", auth)

        self.login("jhon", "NHG0(.")

    def login(self, username, password):
        url = reverse('login')
        data = {'username': username, "password": password}
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual("token" in response.data, True)
        return "JWT %s" % response.data["token"]

    def Test_user_update_password(self, user_id, password, auth):
        url = reverse('user-detail', args=(user_id, ))
        self.client.credentials(HTTP_AUTHORIZATION=auth)
        data = {"password": password}
        response = self.client.patch(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
