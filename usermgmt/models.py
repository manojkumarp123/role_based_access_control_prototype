from django.contrib.auth.models import AbstractUser, \
    UserManager as DjangoUserManager
from django.db import models
from content.utils import ConceptGraph
from django.utils import timezone


class UserManager(DjangoUserManager):
    pass


class User(AbstractUser):
    objects = UserManager()

    def is_entitled(self, subject, activity):
        entitled_concepts = self.entitlements.filter(
            valid_upto__gte=timezone.now(),
            activity=activity).values_list("concept", flat=True)

        for entitled_concept in entitled_concepts:
            if ConceptGraph.is_parent_of_child_or_same(
                    entitled_concept, subject):
                return True
        return False


class Entitlement(models.Model):
    user = models.ForeignKey(
        User, related_name="entitlements")
    valid_upto = models.DateTimeField()
    quantity = models.PositiveSmallIntegerField()
    activity = models.CharField(max_length=20)
    concept = models.CharField(max_length=50)
