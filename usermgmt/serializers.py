from rest_framework import serializers
from .models import User, Entitlement


class EntitlementSerializer(serializers.ModelSerializer):
    class Meta:
        fields = "__all__"
        model = Entitlement


class UserSerializer(serializers.ModelSerializer):
    entitlements = EntitlementSerializer(many=True, read_only=True)

    def create(self, validated_data):
        user = User.objects.create_user(**validated_data)
        return user

    def update(self, instance, validated_data):
        """
        Explicit call to user.set_password()
        """
        # Restrict update of username
        validated_data.pop("username", None)

        password = validated_data.pop("password", None)
        user = super(UserSerializer, self).update(instance, validated_data)
        if password:
            user.set_password(password)
            user.save()
        return user

    class Meta:
        model = User
        fields = (
            "id", "username", "password", "first_name", "last_name",
            "email", "entitlements")
        extra_kwargs = {"password": {"write_only": True}}
