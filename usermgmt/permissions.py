from rest_framework.permissions import BasePermission


class UserPermission(BasePermission):
    def has_object_permission(self, request, view, user):
        """
        Allow logged in user to update his profile
        """
        if request.user == user and view.action != "destroy":
            return True
        else:
            return request.user.is_superuser
