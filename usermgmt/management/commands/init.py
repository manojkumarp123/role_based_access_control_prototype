from django.core.management.base import BaseCommand
from usermgmt.models import User


class Command(BaseCommand):
    help = 'Creates an admin user'

    def handle(self, *args, **options):
        if User.objects.create_superuser(
                email="admin@entrayn.com", username="admin",
                password="entrayn1234"):
            self.stdout.write(self.style.SUCCESS(
                'Successfully created super user'))
        else:
            self.stdout.write(self.style.ERROR(
                'Unable to create super user'))

        if User.objects.create_user(
                username="test", password="entrayn1234"):
            self.stdout.write(self.style.SUCCESS(
                'Successfully created test user'))
        else:
            self.stdout.write(self.style.ERROR(
                'Unable to create test user'))
