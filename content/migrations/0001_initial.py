# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2017-04-13 11:24
from __future__ import unicode_literals

import django.contrib.postgres.fields.jsonb
from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Question',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('text', models.CharField(max_length=200)),
                ('answer_choices', django.contrib.postgres.fields.jsonb.JSONField()),
                ('subject', models.CharField(max_length=50)),
            ],
        ),
    ]
