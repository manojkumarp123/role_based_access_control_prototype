class ConceptGraph(object):
    ALL = "ALL"
    GRE = "GRE"
    QUANT = "QUANT"
    ALGEBRA = "ALGEBRA"
    ARITHEMETIC = "ARITHEMETIC"
    VERBAL = "VERBAL"
    SAT = "SAT"
    CONCEPTS = [
      {
        "name": ALL,
        "children": [
          {
            "name": GRE,
            "children": [
              {
                "name": QUANT,
                "children": [
                  {
                    "name": ALGEBRA,
                    "children": None
                  },
                  {
                    "name": ARITHEMETIC,
                    "children": None
                  }
                ]
              },
              {
                "name": VERBAL,
                "children": None
              }
            ]
          },
          {
            "name": SAT,
            "children": None
          }
        ]
      }
    ]

    @staticmethod
    def get_all_graph_concepts(concepts=CONCEPTS):
        list_of_concepts = []
        for concept in concepts:
            list_of_concepts.append(concept["name"])

            if concept["children"]:
                list_of_concepts.extend(
                    ConceptGraph.get_all_graph_concepts(concept["children"]))

        return list_of_concepts

    @staticmethod
    def find_graph(concepts, subject):
        for concept in concepts:
            # print(concept, subject)
            if concept["name"] == subject:
                return (True,  concept["children"])

            if concept["children"]:
                found, sub_graph = ConceptGraph.find_graph(
                    concept["children"], subject)
                if found:
                    return found, sub_graph
        return False, None

    @staticmethod
    def is_parent_of_child_or_same(parent_concept, child_concept):
        resource_present, sub_graph = ConceptGraph.find_graph(
            ConceptGraph.CONCEPTS, parent_concept)
        if resource_present and parent_concept == child_concept:
            return True
        elif sub_graph:
            child_of_parent, user_concent_graph = ConceptGraph.find_graph(
                sub_graph, child_concept)
            return child_of_parent
        return False


class Activity(object):
    TEST = "test"
    TEACH = "teach"
    LEARN = "learn"
    DOUBT = "doubt"
