from rest_framework.permissions import BasePermission
from .utils import Activity


class QuestionPermission(BasePermission):
    """
    Allow users with entitlements access
    """
    def has_object_permission(self, request, view, question):
        if request.user == question.author:
            return True
        if view.action == "retrieve" and \
                request.user.is_entitled(question.subject, Activity.LEARN):
            return True
        return False
