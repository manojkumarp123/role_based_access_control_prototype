from .views import QuestionViewSet
from rest_framework.routers import SimpleRouter

router = SimpleRouter()
router.register('questions', QuestionViewSet)
urlpatterns = router.urls
