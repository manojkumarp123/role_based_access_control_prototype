from django.db import models
from django.contrib.postgres.fields import JSONField
from usermgmt.models import User


class QuestionManager(models.Manager):
    pass


class Question(models.Model):
    text = models.CharField(max_length=200)
    answer_choices = JSONField()
    author = models.ForeignKey(
        User, related_name="questions_authored")
    assigned_to = models.ForeignKey(
        User, related_name="questions_assigned")
    subject = models.CharField(max_length=50)

    objects = QuestionManager()
