from rest_framework import viewsets
from .serializers import QuestionSeriazlier
from .models import Question
from .permissions import QuestionPermission


class QuestionViewSet(viewsets.ModelViewSet):
    serializer_class = QuestionSeriazlier
    queryset = Question.objects.all()
    permission_classes = (QuestionPermission, )
