from django.test import TestCase
from .utils import ConceptGraph
from rest_framework.test import APITestCase
from django.utils import timezone
from datetime import timedelta
from content.utils import Activity
from .models import User
from django.urls import reverse
from content.models import Question
from rest_framework import status


class ConceptGraphTestCase(TestCase):
    def test_top_concept(self):
        self.assertEquals(
            ConceptGraph.is_parent_of_child_or_same(
                ConceptGraph.ALL, ConceptGraph.SAT), True)
        self.assertEquals(
            ConceptGraph.is_parent_of_child_or_same(
                ConceptGraph.ALL, ConceptGraph.GRE), True)
        self.assertEquals(
            ConceptGraph.is_parent_of_child_or_same(
                ConceptGraph.ALL, ConceptGraph.ALL), True)

    def test_non_existant_top_concept(self):
        self.assertEquals(
            ConceptGraph.is_parent_of_child_or_same(
                "ALL2", ConceptGraph.ALL), False)

    def test_gre(self):
        self.assertEquals(
            ConceptGraph.is_parent_of_child_or_same(
                ConceptGraph.GRE, ConceptGraph.VERBAL), True)
        self.assertEquals(
            ConceptGraph.is_parent_of_child_or_same(
                ConceptGraph.GRE, ConceptGraph.QUANT), True)
        self.assertEquals(
            ConceptGraph.is_parent_of_child_or_same(
                ConceptGraph.GRE, ConceptGraph.ARITHEMETIC), True)
        self.assertEquals(
            ConceptGraph.is_parent_of_child_or_same(
                ConceptGraph.GRE, ConceptGraph.ALGEBRA), True)

    def test_quant(self):
        self.assertEquals(
            ConceptGraph.is_parent_of_child_or_same(
                ConceptGraph.QUANT, ConceptGraph.ARITHEMETIC), True)
        self.assertEquals(
            ConceptGraph.is_parent_of_child_or_same(
                ConceptGraph.QUANT, ConceptGraph.ALGEBRA), True)

    def test_verbal_negative_case(self):
        self.assertEquals(
            ConceptGraph.is_parent_of_child_or_same(
                ConceptGraph.VERBAL, ConceptGraph.ALGEBRA), False)

    def test_general(self):
        self.assertEquals(
            ConceptGraph.is_parent_of_child_or_same(
                ConceptGraph.ALL, ConceptGraph.ARITHEMETIC), True)
        self.assertEquals(
            ConceptGraph.is_parent_of_child_or_same(
                ConceptGraph.ALL, ConceptGraph.ALGEBRA), True)


class QuestionConsumptionAPITestCase(APITestCase):
    def setUp(self):
        self.author = User.objects.create_user(
            "author", password="entrayn1234")
        self.author.entitlements.create(
            valid_upto=timezone.now() + timedelta(days=1),
            concept=ConceptGraph.QUANT, activity=Activity.TEACH,
            quantity=3)

        # Create Questions in GRE, QUANT, VERBAL, ALGEBRA and ARETHMETIC
        # Create learners
        self.learners = []
        self.questions = []
        concepts = ConceptGraph.get_all_graph_concepts()
        for concept in concepts:
            learner = self.create_learner(concept)
            question = Question.objects.create(
                text="question text", answer_choices={},
                author=self.author, assigned_to=learner,
                subject=concept)
            self.learners.append(learner)
            self.questions.append(question)

    def create_learner(self, concept=ConceptGraph.ALGEBRA):
        learner = User.objects.create_user(
            "learner_%s" % concept, password="entrayn1234")
        learner.entitlements.create(
            valid_upto=timezone.now() + timedelta(days=1),
            concept=concept, activity=Activity.LEARN,
            quantity=3)
        return learner

    def test_question_consumption(self):
        for question in self.questions:
            url = reverse('question-detail', args=(question.id,))
            for learner in self.learners:
                self.client.force_authenticate(learner)
                response = self.client.get(url, format='json')
                is_response_expected = ConceptGraph.is_parent_of_child_or_same(
                        learner.entitlements.first().concept,
                        question.subject)
                print(
                      "question:%s" % question.subject,
                      learner.username,
                      is_response_expected,
                      response.status_code)
                if is_response_expected:
                    self.assertEqual(response.status_code, status.HTTP_200_OK)
                else:
                    self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
