from rest_framework import serializers
from .models import Question
from .utils import ConceptGraph, Activity
from django.utils import timezone


class AnswerChoiceSerializer(serializers.Serializer):
    id = serializers.IntegerField(read_only=True)
    text = serializers.CharField(max_length=200)
    correct = serializers.BooleanField()


class QuestionSeriazlier(serializers.ModelSerializer):
    answer_choices = AnswerChoiceSerializer(many=True)
    author = serializers.HiddenField(
        default=serializers.CurrentUserDefault())

    class Meta:
        model = Question
        fields = (
            "id", "text", "answer_choices", "author", "assigned_to",
            "subject")

    def create(self, validated_data):
        answer_choices = []
        for idx, answer_choice in enumerate(validated_data["answer_choices"]):
            answer_choices.append(dict(answer_choice, id=idx))
        validated_data["answer_choices"] = answer_choices
        return Question.objects.create(**validated_data)

    def validate_answer_choices(self, value):
        if len(value) != 4:
            raise serializers.ValidationError('Length not equal to 4.')
        return value

    def validate_subject(self, subject):
        if subject not in ConceptGraph.get_all_graph_concepts():
            raise serializers.ValidationError('Invalid subject')
        return subject

    def is_author_entitled(self, author, subject):
        entitled_concepts = author.entitlements.filter(
            valid_upto__gte=timezone.now(),
            activity=Activity.TEACH).values_list("concept", flat=True)
        for entitled_concept in entitled_concepts:
            if ConceptGraph.is_parent_of_child_or_same(
                    entitled_concept, subject):
                return True
        return False

    def is_learner_entitled_to_consume(self, learner, subject):
        return True

    def validate(self, attrs):
        validated_data = super(QuestionSeriazlier, self).validate(attrs)
        subject = validated_data["subject"]
        # 1. Learner should be entitled to take the concept
        # 2. Author should be entitled to add in concept
        author = validated_data["author"]
        if not self.is_author_entitled(author, subject):
            raise serializers.ValidationError(
                'Author is not entitled to add question in subject')
        learner = validated_data["assigned_to"]
        if not self.is_learner_entitled_to_consume(learner, subject):
            raise serializers.ValidationError(
                'Learner is not entitled to consume question in subject')

        return validated_data
